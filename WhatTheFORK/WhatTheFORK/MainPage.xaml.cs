﻿using System;
using System.Collections.Generic;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Plugin.Media;
using System.Net.Http;
using System.Net.Http.Headers;

namespace WhatTheFORK
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            take.Clicked += CameraButton_Clicked;
            pick.Clicked += btnPick_Clicked;
        }

        static string subscriptionKey = Environment.GetEnvironmentVariable("COMPUTER_VISION_SUBSCRIPTION_KEY");
        static string endpoint = Environment.GetEnvironmentVariable("COMPUTER_VISION_ENDPOINT");
        static string uriBase = endpoint + "vision/v2.0/analyze";



        private async void CameraButton_Clicked(object sender, EventArgs e)
        {
            var photo = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions() { });

            //string imageFilePath = Console.ReadLine();

            if (photo != null)
                image.Source = ImageSource.FromStream(() => { return photo.GetStream(); });

            //MakeAnalysisRequest(imageFilePath).Wait();
        }

        private async void btnPick_Clicked(object sender, EventArgs e)
        {
            var file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium
            });

            //string imageFilePath = Console.ReadLine();

            if (file == null)
                return;
            image.Source = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                return stream;
            });

            //MakeAnalysisRequest(imageFilePath).Wait();
        }

        static async Task MakeAnalysisRequest(string imageFilePath)
        {
            try
            {
                HttpClient client = new HttpClient();

                // Request headers.
                client.DefaultRequestHeaders.Add(
                    "Ocp-Apim-Subscription-Key", subscriptionKey);

                // Request parameters. A third optional parameter is "details".
                // The Analyze Image method returns information about the following
                // visual features:
                // Categories:  categorizes image content according to a
                //              taxonomy defined in documentation.
                // Description: describes the image content with a complete
                //              sentence in supported languages.
                // Color:       determines the accent color, dominant color, 
                //              and whether an image is black & white.
                string requestParameters = "visualFeatures=Categories,Description";

                // Assemble the URI for the REST API method.
                string uri = uriBase + "?" + requestParameters;

                HttpResponseMessage response;

                // Read the contents of the specified local image
                // into a byte array.
                byte[] byteData = GetImageAsByteArray(imageFilePath);

                // Add the byte array as an octet stream to the request body.
                using (ByteArrayContent content = new ByteArrayContent(byteData))
                {
                    // This example uses the "application/octet-stream" content type.
                    // The other content types you can use are "application/json"
                    // and "multipart/form-data".
                    content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");

                    // Asynchronously call the REST API method.
                    response = await client.PostAsync(uri, content);
                }

                // Asynchronously get the JSON response.
                string contentString = await response.Content.ReadAsStringAsync();

                // Display the JSON response.
                Console.WriteLine("\nResponse:\n\n{0}\n",
                    JToken.Parse(contentString).ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("\n" + e.Message);
            }
        }

        /// <summary>
        /// Returns the contents of the specified file as a byte array.
        /// </summary>
        /// <param name="imageFilePath">The image file to read.</param>
        /// <returns>The byte array of the image data.</returns>
        static byte[] GetImageAsByteArray(string imageFilePath)
        {
            // Open a read-only file stream for the specified file.
            using (FileStream fileStream =
                new FileStream(imageFilePath, FileMode.Open, FileAccess.Read))
            {
                // Read the file's contents into a byte array.
                BinaryReader binaryReader = new BinaryReader(fileStream);
                return binaryReader.ReadBytes((int)fileStream.Length);
            }
        }


        //private const string subscriptionKey = "7be061ec42514cab8849284b3c4829e4";
        //private const string endpoint = "https://compvis4581.cognitiveservices.azure.com/";

        //public async Task<ImageAnalysis> GetImageDescription(Stream imageStream)
        //{
        //    ComputerVisionClient visionClient = new ComputerVisionClient(subscriptionKey, endpoint);
        //    VisualFeatureTypes[] features = { VisualFeatureTypes.Tags, VisualFeatureTypes.Categories, VisualFeatureTypes.Description };
        //    return await visionClient.AnalyzeImageAsync(imageStream, features.ToList(), null);
        //}

        //private async void btnPick_Clicked(object sender, EventArgs e)
        //{
        //    await CrossMedia.Current.Initialize();
        //    try
        //    {
        //        var file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
        //        {
        //            PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium
        //        });
        //        if (file == null)
        //            return;
        //        image.Source = ImageSource.FromStream(() =>
        //        {
        //            var stream = file.GetStream();
        //            return stream;
        //        });
        //        var result = await GetImageDescription(file.GetStream());
        //        lblResult.Text = null;
        //        file.Dispose();
        //        foreach (string tag in result.Description.Tags)
        //        {
        //            lblResult.Text = lblResult.Text + "\n" + tag;
        //        }
        //    }
        //    catch
        //    (Exception ex)
        //    {
        //        string test = ex.Message;
        //    }
        //}

        //private async void CameraButton_Clicked(object sender, EventArgs e)
        //{
        //    await CrossMedia.Current.Initialize();
        //    try
        //    {
        //        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
        //        {
        //            await DisplayAlert("No Camera", ":( No camera available.", "OK");
        //            return;
        //        }
        //        var photo = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions() { });

        //        if (photo != null)
        //            image.Source = ImageSource.FromStream(() => { return photo.GetStream();
        //        });
        //        var result = await GetImageDescription(photo.GetStream());
        //        lblResult.Text = null;
        //        photo.Dispose();
        //        foreach (string tag in result.Description.Tags)
        //        {
        //            lblResult.Text = lblResult.Text + "\n" + tag;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string test = ex.Message;
        //    }
        //}
    }
}
